package io.androidovshchik.blankproject

import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.github.androidovshchik.support.BaseV7Activity
import io.androidovshchik.switchpictures.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : BaseV7Activity() {

    private var infinity = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        GlideApp.with(applicationContext)
            .asGif()
            .load(R.raw.gifka)
            .into(foreground)
        background.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    infinity = true
                    (foreground.drawable as GifDrawable).stop()
                    (foreground.drawable as GifDrawable).startFromFirstFrame()
                    foreground.visibility = View.VISIBLE
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    Handler().postDelayed({
                        infinity = false
                    }, 200)
                }
            }
            true
        }
        disposable.add(Observable.interval(0, 20, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .subscribe {
                if (!infinity && foreground.drawable != null) {
                    if ((foreground.drawable as GifDrawable).frameIndex ==
                        (foreground.drawable as GifDrawable).frameCount - 1 &&
                        foreground.visibility != View.INVISIBLE) {
                        (foreground.drawable as GifDrawable).stop()
                        foreground.visibility = View.INVISIBLE
                    }
                }
            })
    }
}
