package io.androidovshchik.blankproject

import android.content.Context
import com.github.androidovshchik.core.utils.context.appContext
import com.github.androidovshchik.core.utils.context.isDebug
import com.github.androidovshchik.support.BaseSApplication

@Suppress("unused")
class MainApplication : BaseSApplication() {

    override fun onCreate() {
        super.onCreate()
        if (isDebug()) {
            Class.forName("com.facebook.stetho.Stetho")
                .getDeclaredMethod("initializeWithDefaults", Context::class.java)
                .invoke(null, appContext)
        }
    }
}